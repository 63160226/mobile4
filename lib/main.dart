import 'package:flutter/material.dart';
enum APP_THEME {LIGHT,DARK}
void main() {
runApp(ContactProfilePage());
}
class MyAppTheme {
static ThemeData appThemeLight() {
return ThemeData(
brightness: Brightness.light,
appBarTheme: AppBarTheme(
color: Colors.white,
iconTheme: IconThemeData(
color: Colors.black
)
),
iconTheme: IconThemeData(
color: Colors.indigo.shade500
)
);
}
static ThemeData appThemeDark() {
return ThemeData(
brightness: Brightness.dark
);
}
}
class ContactProfilePage extends StatefulWidget{
@override
State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;
@override
Widget build(BuildContext context){
return MaterialApp(
debugShowCheckedModeBanner: false,
theme: currentTheme == APP_THEME.DARK
    ? MyAppTheme.appThemeLight()
    : MyAppTheme.appThemeDark(),
home: Scaffold(
appBar: buildAppBarWidget(),
body: buildBodyWidget(),
  floatingActionButton: FloatingActionButton(
    child:  Icon(Icons.threesixty),
    onPressed: (){
      setState(() {
        currentTheme == APP_THEME.DARK
            ?currentTheme = APP_THEME.LIGHT
            : currentTheme = APP_THEME.DARK;
      });
    },

  ),
),

);
}
}
Widget profileActionItem(){
return Row(
mainAxisAlignment: MainAxisAlignment.spaceEvenly,
children: <Widget>[
buildCallButton(),
buildTextButton(),
buildVideoCallButton(),
buildEmailButton(),
buildDiretionsButton(),
buildPayButton(),
],
);
}
Widget buildCallButton() {
return Column(
children: <Widget>[
IconButton(
icon: Icon(
Icons.call,
//     color: Colors.indigo.shade800,
),
onPressed: () {},
),
Text("Call"),
],
);
}
Widget buildTextButton() {
return Column(
children: <Widget>[
IconButton(
icon: Icon(
Icons.message,
//     color: Colors.indigo.shade800,
),
onPressed: () {},
),
Text("Text"),
],
);
}
Widget buildVideoCallButton() {
return Column(
children: <Widget>[
IconButton(
icon: Icon(
Icons.video_call,
//       color: Colors.indigo.shade800,
),
onPressed: () {},
),
Text("Video"),
],
);
}
Widget buildEmailButton() {
return Column(
children: <Widget>[
IconButton(
icon: Icon(
Icons.email,
//      color: Colors.indigo.shade800,
),
onPressed: () {},
),
Text("Email"),
],
);
}
Widget buildDiretionsButton() {
return Column(
children: <Widget>[
IconButton(
icon: Icon(
Icons.directions,
//     color: Colors.indigo.shade800,
),
onPressed: () {},
),
Text("Directions"),
],
);
}
Widget buildPayButton() {
return Column(
children: <Widget>[
IconButton(
icon: Icon(
Icons.attach_money_outlined,
//       color: Colors.indigo.shade800,
),
onPressed: () {},
),
Text("Pay"),
],
);
}
Widget mobilePhoneListTile(){
return ListTile(
leading: Icon(Icons.call),
title: Text('0983876861'),
subtitle: Text('mobile'),
trailing:IconButton(
icon: Icon(Icons.message),
color: Colors.amber,
onPressed: (){},
),
);

}
Widget otherPhoneListTile(){
return ListTile(
leading: Text(""),
title: Text('440-440-2556'),
subtitle: Text('other'),
trailing:IconButton(
icon: Icon(Icons.message),
color: Colors.amber,
onPressed: (){},
),
);

}
Widget emailListTile(){
return ListTile(
leading: Icon(Icons.email),
title: Text('63160226@go.buu.ac.th'),
subtitle: Text('work'),
trailing: Text("")

);

}
Widget addressListTile(){
return ListTile(
leading: Icon(Icons.location_on),
title: Text('Bang saen'),
subtitle: Text('home'),
trailing: IconButton(
icon: Icon(Icons.assistant_direction),
color: Colors.amberAccent,
onPressed: (){},
),

);

}
AppBar buildAppBarWidget() {
return AppBar(
backgroundColor: Colors.redAccent,

leading: Icon(
Icons.arrow_back,
color: Colors.white,
),
actions: <Widget>[
IconButton(
onPressed: (){},
icon: Icon(Icons.star_border),
color: Colors.white
)
],
);
}
Widget buildBodyWidget(){
return ListView(

children: <Widget>[
Column(
children:  <Widget>[
Container(
width: double.infinity,

//Height constraint at Container widget level
height: 250,

child:

Image.network(
"https://reg.buu.ac.th/registrar/getstudentimage.asp?id=63160226",
fit: BoxFit.cover,
),
),
Container(
height: 60,
child: Row(
mainAxisAlignment: MainAxisAlignment.start,
children: <Widget>[
Padding(
padding: EdgeInsets.all(8.0),
child: Text(" Satit Wapeetao",
style : TextStyle(fontSize: 30),

),
),
],
),
),
Divider(
color: Colors.grey,
),

Container(
margin: EdgeInsets.only(top: 8,bottom: 8),
child: Theme(
data: ThemeData(
iconTheme: IconThemeData(
color: Colors.pink,
),
),
child : profileActionItem(),
),
),


Divider(
color: Colors.grey,
),
mobilePhoneListTile(),
otherPhoneListTile(),
Divider(
color:  Colors.grey,

),
emailListTile(),
addressListTile()
],
)
],
);
}